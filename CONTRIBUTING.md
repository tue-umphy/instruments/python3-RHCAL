# Contributing to `RHCAL`

If you have found a bug or have an idea to improve `RHCAL`, [open an issue on
GitLab](https://gitlab.com/tue-umphy/instruments/python3-RHCAL/issues/new) and
describe your thoughts.
