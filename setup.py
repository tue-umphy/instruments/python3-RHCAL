#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# System modules
import os
import runpy
from setuptools import setup, find_packages


def read_file(filename):
    with open(filename) as f:
        return f.read()


# run setup
setup(
    name="RHCAL",
    description="Operate the RH CAL calibrator via a serial port",
    author="Yann Büchau",
    author_email="nobodyinperson@gmx.de",
    keywords="measurement,logging,calibration",
    license="GPLv3",
    version=runpy.run_path("RHCAL/version.py").get("__version__", "0.0.0"),
    url="https://gitlab.com/tue-umphy/instruments/python3-RHCAL",
    long_description=read_file("README.md"),
    classifiers=[
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Programming Language :: Python :: 3.5",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
    install_requires=["pyserial"],
    packages=find_packages(),
    entry_points={"console_scripts": ["rhcal = RHCAL.__main__:main"]},
)
