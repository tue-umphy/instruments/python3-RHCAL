# 🌡 Operating the EdgeTech RH CAL Relative Humidity Calibrator

This software includes the Python module `RHCAL` which provides utilities to
interface an [EdgeTech RH CAL Relative Humidity Calibrator]
(https://edgetechinstruments.com/product/rh-cal-portable-relative-humidity-calibrator/).

## 📥 Installation

```bash
pip install git+https://gitlab.com/tue-umphy/instruments/python3-RHCAL.git
```

## 🔧 Usage

```bash
rhcal

# if that says 'command not found', try:
python3 -m RHCAL
```

### ❓ Help

The CLI provides a detailed help page which can be accessed via

```bash
rhcal -h
```

Every possible option is explained there.

### 📝 Logging

To simply display data from a connected EdgeTech RH CAL Relative Humidity
Calibrator into a CSV-file `rhcal.csv`, run

```bash
# show output in terminal
rhcal 

# Also show under-the-hood communication
rhcal -v

# log to file
rhcal -o rhcal.csv

# log to file and still see output
rhcal | tee rhcal.csv
```

The logged data also reflects manual changes performed via the keypad on the
device.

Example `rhcal.csv` output file:

```conf
# RHCAL/__main__.py -p /dev/ttyUSB1 -o rhcal.csv
time_log_local,time_rhcal,temp_c,temp_setpoint_c,dewpoint_c,hum_percent,hum_setpoint_percent,mode
2018-07-13 12:33:11,2018-07-13 12:33:11,29.0,20.0,9.8,30.3,61.0,ABC CYCLE
2018-07-13 12:33:13,2018-07-13 12:33:13,29.0,20.0,10.8,32.3,61.0,ABC CYCLE
2018-07-13 12:33:15,2018-07-13 12:33:15,29.0,20.0,11.7,34.3,61.0,ABC CYCLE
2018-07-13 12:33:17,2018-07-13 12:33:17,29.0,20.0,12.7,36.7,61.0,ABC CYCLE
2018-07-13 12:33:19,2018-07-13 12:33:19,29.0,20.0,13.6,38.9,61.0,ABC CYCLE
2018-07-13 12:33:21,2018-07-13 12:33:21,29.0,20.0,14.5,41.2,61.0,ABC CYCLE
2018-07-13 12:33:23,2018-07-13 12:33:23,28.9,20.0,15.4,43.8,61.0,ABC CYCLE
2018-07-13 12:33:25,2018-07-13 12:33:25,28.9,20.0,16.2,46.2,61.0,ABC CYCLE
2018-07-13 12:33:27,2018-07-13 12:33:27,28.9,20.0,17.1,49.0,61.0,ABC CYCLE
```

### 🧪  Experiments

It is also possible to run multiple experiments with different settings.

The experiments are defined in a CSV file. The experiments are processed with

```bash
rhcal -f /path/to/experiment/file.csv
```

To save the CSV output into a file, specify also `-o output.csv`.

#### 📅 Columns in the Experiment file

The Experiment file can have the following columns, all of which are _optional_ as
they have defaults.

|**Column**|`start_time`|`end_time`|`min_seconds_at_setpoint`|`max_seconds`|`temperature_c`|`humidity_percent`|`temperature_tolerance_c`|`humidity_tolerance_percent`|`output_interval`|
|---|----|---|-|-|-|-|-|-|-|
|**Description**|wait until this time to start the calibration step|calibration maximum end time|minimum time the setpoint should have been reached within the tolerance region|maximum time this calibration step should be attempted|temperature setpoint|humidity setpoint|temperature tolerance|humidity tolerance|the output interval for this calibration step|
|**Default**|now|now + `max_seconds`| $`\infty`$ | $`\infty`$ | no change | no change | $`\infty`$ | $`\infty`$ | no change |
|**Example**|`2018-07-15 10:00:00`|`2018-07-15 11:00:00`|`120`|`600`|`32.4`|`55.5`|`0.5`|`5`|`1`|

#### Example

For example, an minimal experiment file `experiments.csv`:

```conf
# calibration file
# comments starting with '#' and empty lines are allowed
# the first line is the header line
min_seconds_at_setpoint,temperature_c,temperature_tolerance_c,humidity_percent,humidity_tolerance_percent
# produce 900 seconds (15 minutes) of data at 20°C (±1°C) and 50% humidity (±5%)
900,20,1,50,5
# produce 1800 seconds (30 minutes) of data at 25°C (±2°C) and 60% humidity (±7%)
1800,25,2,60,7
# produce 3600 seconds (1 hour) of data at 30°C (±3°C) and 40% humidity (±8%)
3600,30,3,40,8
```

There is also a _„dry-run”_ mode (`-n` or `--dry_run`) which doesn't actually
do anything with the device but just simulates what would be done. This is
handy to check what an experiment file would do:

```
python3 -m RHCAL -f experiments.csv -n
```

The output would be:

```
WARNING:RHCAL cli:Performing dry run. None of the following actions are actually preformed and all displayed data is fake.
INFO:RHCAL cli:Experiment Nr. 1
# Experiment Nr. 1
INFO:RHCAL cli:Setting temperature setpoint to 20°C
INFO:RHCAL cli:Setting humidity setpoint to 50%
INFO:RHCAL cli:Will now log data until temperature (20±1°C) and humidity (50±5%) are at least 900 seconds within their setpoint tolerances (but at most an infinity of seconds) or until 9999-12-31 23:59:59.999999...
INFO:RHCAL cli:Experiment Nr. 2
# Experiment Nr. 2
INFO:RHCAL cli:Setting temperature setpoint to 25°C
INFO:RHCAL cli:Setting humidity setpoint to 60%
INFO:RHCAL cli:Will now log data until temperature (25±2°C) and humidity (60±7%) are at least 1800 seconds within their setpoint tolerances (but at most an infinity of seconds) or until 9999-12-31 23:59:59.999999...
INFO:RHCAL cli:Experiment Nr. 3
# Experiment Nr. 3
INFO:RHCAL cli:Setting temperature setpoint to 30°C
INFO:RHCAL cli:Setting humidity setpoint to 40%
INFO:RHCAL cli:Will now log data until temperature (30±3°C) and humidity (40±8%) are at least 3600 seconds within their setpoint tolerances (but at most an infinity of seconds) or until 9999-12-31 23:59:59.999999...
```

### Interactively

To control the RH CAL interactively, you may use `ipython3` (install it with
`pip3 install --user -U ipython`) and set up an `RHCAL` object like this:

```python
from RHCAL import RHCAL
rhcal = RHCAL(port = "/dev/ttyUSB1", baudrate = 57600)
```

You can then use the `rhcal` object to query and set parameters, e.g.

```python
# retreive the device status
rhcal.status
# retreive the current temperature setpoint
rhcal.temperature_setpoint
# set the current temperature setpoint to 25.5 °C
rhcal.temperature_setpoint = 25.5
# the same works for `humidity_setpoint` or the `output_interval`
rhcal.humidity_setpoint = 80 # %
rhcal.output_interval = 2 # seconds
# retreive the next measurements
next(rhcal.data)
# continuously display data
for data in rhcal.data:
    print(data)
# ...
```
