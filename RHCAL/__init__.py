# system modules
import logging
import time
import re
import datetime

# internal modules
from RHCAL.version import __version__
from RHCAL.utils import *

# external modules
import serial

logger = logging.getLogger(__name__)


class RHCAL(serial.Serial):
    BAUDRATES = {1200, 2400, 4800, 9600, 19200, 57600}  # available baudrates
    TYPE_DELAY_SECONDS = 0.3  # the time to wait between simulated keystrokes
    RESPONSE_DELAY_SECONDS = 0.5  # the time to wait for a device response
    ENCODING = "ascii"
    DATE_FORMAT = "%m/%d/%y"
    TIME_FORMAT = "%H:%M:%S"
    DATETIME_FORMAT = "{} {}".format(DATE_FORMAT, TIME_FORMAT)
    STATUS_CACHE_SECONDS = 60  # how long the status is chached
    """
    Class Constructor

    This constructor is basically the same as for :any:`serial.Serial` except
    that it sets some defaults.
    """

    def __init__(self, *args, **kwargs):
        if "baudrate" not in kwargs:
            kwargs.update(baudrate=57600)
        if "bytesize" not in kwargs:
            kwargs.update(bytesize=serial.EIGHTBITS)
        if "stopbits" not in kwargs:
            kwargs.update(stopbits=serial.STOPBITS_ONE)
        if "parity" not in kwargs:
            kwargs.update(parity=serial.PARITY_NONE)
        serial.Serial.__init__(self, *args, **kwargs)

    @property
    def output_interval(self):
        """
        Data output interval in seconds. Between ``1`` and ``3600``.

        :getter: Extracts the output interval from :any:`RHCAL.status`.
        :setter: Sets the output interval in the device
        :type: int
        """
        interval = self.status.get("SERIAL", {}).get("Interval")
        seconds = int(
            (
                datetime.datetime.strptime(interval, "%M:%S")
                - datetime.datetime.strptime("0:00", "%M:%S")
            ).total_seconds()
        )
        return seconds

    @output_interval.setter
    def output_interval(self, seconds):
        self.command("o")
        response = self.bytes2str(self.read_all())
        logger.debug("<<< {}".format(response))
        regex = re.compile(
            r"serial.*interval.*is.*?(?P<seconds>\w+)\s+seconds",
            flags=re.IGNORECASE,
        )
        m = regex.search(response)
        try:
            setting = int(m.groupdict().get("seconds"))
            logger.debug("Current interval is {} seconds".format(setting))
        except (AttributeError, ValueError, TypeError):
            raise RHCALException(
                "Entering output interval setting mode " "did not work"
            )
        for s in str(seconds):
            self.type(s)
        self.press_enter()
        time.sleep(self.RESPONSE_DELAY_SECONDS)
        response = self.bytes2str(self.read_all())
        logger.debug("<<< {}".format(response))
        regex = re.compile(
            r"serial.*interval.*is.*?(?P<seconds>\w+)\s+seconds", re.IGNORECASE
        )
        m = regex.search(response)
        try:
            setting = int(m.groupdict().get("seconds"))
            if setting == seconds:
                logger.debug("Interval set to {} seconds".format(setting))
            else:
                raise RHCALException(
                    "Output interval still at {}, not {}".format(
                        setting, seconds
                    )
                )
        except (AttributeError, ValueError, TypeError):
            raise RHCALException(
                "Setting output interval did not work, "
                " device responded bogus"
            )

    @property
    def status(self):
        """
        Device status

        :getter: Requests several device parameters. The values are cached for
            :any:`RHCAL.STATUS_CACHE_SECONDS`.
        :type: :any:`dict`
        """
        # make sure last status check time is present
        try:
            self._last_status_check
        except AttributeError:
            self._last_status_check = time.time() - self.STATUS_CACHE_SECONDS
        # delete cached state if last check was too long ago
        if hasattr(self, "_cached_status_dict"):
            if (
                time.time() - self._last_status_check
                >= self.STATUS_CACHE_SECONDS
            ):
                logger.debug("Last status check too long ago. Dropping cache.")
                del self._cached_status_dict
        try:
            # check if we have a cache
            self._cached_status_dict
        except AttributeError:
            # no cache present -> request status
            logger.debug("No status cache present. Requesting status.")
            self.command("st")
            status_string = self.read_until_regex(
                re.compile(
                    r"Press\s+ENTER\s+to\s+continue\.{5,}$",
                    flags=re.IGNORECASE,
                )
            )
            self.press_enter()
            sections_regex = re.compile(
                pattern=r"(\w+)\s+data:\s+([^\r\n]+)\s+([^\r\n]+)\s+",
                flags=re.IGNORECASE,
            )
            sections = sections_regex.findall(status_string)
            try:
                self._cached_status_dict = {
                    section[0]: {
                        k: v
                        for k, v in zip(
                            re.split(r"\s{2,}", section[1].strip()),
                            re.split(r"\s{2,}", section[2].strip()),
                        )
                    }
                    for section in sections
                }
                assert self._cached_status_dict, "empty status dict"
                self._last_status_check = time.time()
            except Exception as e:
                raise RHCALException(
                    "Could not parse status '{}': {}".format(status_string, e)
                )
        return self._cached_status_dict

    @property
    def time(self):
        """
        The sensor time

        :getter: retreive the current device time
        :setter: set the device time
        :type: :any:`datetime.datetime`
        """
        # get date
        self.command("D")
        date_pattern = r"Current\s+date:\s+(?P<date>\d{2}/\d{2}/\d{2})$"
        date_regex = re.compile(date_pattern, flags=re.IGNORECASE)
        date_string = self.read_until_regex(date_regex)
        date = date_regex.search(date_string).groupdict().get("date")
        self.press_enter()  # abort date setting mode
        time.sleep(self.RESPONSE_DELAY_SECONDS)
        self.flushInput()  # discard any further output
        # get time
        self.command("T")
        time_pattern = r"Current\s+time:\s+(?P<time>\d{2}:\d{2}:\d{2})$"
        time_regex = re.compile(time_pattern, flags=re.IGNORECASE)
        time_string = self.read_until_regex(time_regex)
        time_string = time_regex.search(time_string).groupdict().get("time")
        self.press_enter()  # abort time setting mode
        time.sleep(self.RESPONSE_DELAY_SECONDS)
        self.flushInput()  # discard any further output
        # convert to datetime
        return datetime.datetime.strptime(
            "{} {}".format(date, time_string),
            self.DATE_FORMAT + " " + self.TIME_FORMAT,
        )

    @time.setter
    def time(self, newtime):
        time_before_setting_date = time.time()
        # set date
        self.command("D")
        self.type(newtime.strftime(self.DATE_FORMAT))
        self.press_enter()
        time.sleep(self.RESPONSE_DELAY_SECONDS)
        date_pattern = r"The\s+date\s+is:\s+(?P<date>\d{2}/\d{2}/\d{2})$"
        date_regex = re.compile(date_pattern, flags=re.IGNORECASE)
        date_string = self.read_until_regex(date_regex)
        self.flushInput()
        if not date_regex.search(date_string):
            raise RHCALException(
                "Date not set, device responded bogus: {}".format(date_string)
            )
        # set time
        self.command("T")
        # compensate date setting time
        newtime += datetime.timedelta(
            seconds=(time.time() - time_before_setting_date)
        )
        # compensate time typing time
        newtime += datetime.timedelta(
            seconds=len(newtime.strftime(self.TIME_FORMAT))
            * self.TYPE_DELAY_SECONDS
        )
        self.type(newtime.strftime(self.TIME_FORMAT))
        self.press_enter()
        time.sleep(self.RESPONSE_DELAY_SECONDS)
        time_pattern = r"The\s+time\s+is:\s+(?P<time>\d{2}:\d{2}:\d{2})$"
        time_regex = re.compile(time_pattern, flags=re.IGNORECASE)
        time_string = self.read_until_regex(time_regex)
        self.flushInput()
        if not time_regex.search(time_string):
            raise RHCALException(
                "Time not set, device responded bogus: {}".format(time_string)
            )

    @property
    def humidity_setpoint(self):
        """
        Humidity Setpoint in percent

        :getter: Extracts the humidity setpoint from :any:`RHCAL.status`.
        :setter: Sets the humidity setpoint in the device
        :type: float
        """
        sp = self.status.get("SETPOINT", {}).get("RH")
        if sp:
            sp = re.sub(pattern="[^0-9.]", repl="", string=sp)
            return float(sp)
        else:
            raise RHCALException("Could not determine humidity setpoint.")

    @humidity_setpoint.setter
    def humidity_setpoint(self, humidity):
        self.command("cr")
        response = self.bytes2str(self.read_all())
        logger.debug("<<< {}".format(response))
        regex = re.compile(
            r"current\s*setpoint\s*:\s*(?P<humidity>\S+)%", flags=re.IGNORECASE
        )
        m = regex.search(response)
        try:
            setting = float(m.groupdict().get("humidity"))
            logger.debug("Current RH setpoint {}%".format(setting))
        except (AttributeError, ValueError, TypeError):
            raise RHCALException("Entering RH setpoint mode did not work")
        for s in str(humidity):
            self.type(s)
        self.press_enter()
        time.sleep(self.RESPONSE_DELAY_SECONDS)
        response = self.bytes2str(self.read_all())
        logger.debug("<<< {}".format(response))
        regex = re.compile(
            r"new\s*setpoint:\s*(?P<humidity>\S+)\s*%RH", re.IGNORECASE
        )
        m = regex.search(response)
        try:
            setting = float(m.groupdict().get("humidity"))
            if setting == humidity:
                logger.debug("RH setpoint set to {}%".format(setting))
                if hasattr(self, "_last_status_check"):
                    del self._last_status_check  # indicate status has changed
            else:
                raise RHCALException(
                    "RH setpoint still at {}, not {}".format(setting, humidity)
                )
        except (AttributeError, ValueError, TypeError):
            raise RHCALException(
                "Setting RH setpoint did not work,"
                " device responded:\n{}".format(response)
            )

    @property
    def temperature_setpoint(self):
        """
        Temperature Setpoint in degrees Celsius

        :getter: Extracts the temperature setpoint from :any:`RHCAL.status`.
        :setter: Sets the temperature setpoint in the device
        :type: float
        """
        sp = self.status.get("SETPOINT", {}).get("Temp.")
        if sp:
            sp = re.sub(pattern="[^0-9.]", repl="", string=sp)
            return float(sp)
        else:
            raise RHCALException("Could not determine temperature setpoint.")

    @temperature_setpoint.setter
    def temperature_setpoint(self, temperature):
        self.command("ct")
        response = self.bytes2str(self.read_all())
        logger.debug("<<< {}".format(response))
        regex = re.compile(
            r"current\s*setpoint\s*:\s*(?P<temperature>\S+)\s*C",
            flags=re.IGNORECASE,
        )
        m = regex.search(response)
        try:
            setting = float(m.groupdict().get("temperature"))
            logger.debug("Current temperature setpoint {}%".format(setting))
        except (AttributeError, ValueError, TypeError):
            raise RHCALException(
                "Entering temperature setpoint mode" " did not work"
            )
        for s in str(temperature):
            self.type(s)
        self.press_enter()
        time.sleep(self.RESPONSE_DELAY_SECONDS)
        response = self.bytes2str(self.read_all())
        logger.debug("<<< {}".format(response))
        regex = re.compile(
            r"new\s*setpoint:\s*(?P<temperature>\S+)\s*C", re.IGNORECASE
        )
        m = regex.search(response)
        try:
            setting = float(m.groupdict().get("temperature"))
            if setting == temperature:
                logger.debug("Temperature setpoint now {} C".format(setting))
                del self._last_status_check  # indicate status has changed
            else:
                raise RHCALException(
                    "Temperature setpoint still "
                    "at {}, not {}".format(setting, temperature)
                )
        except (AttributeError, ValueError, TypeError):
            raise RHCALException(
                "Setting temperature setpoint did not work,"
                " device responded:\n{}".format(response)
            )

    TIME_PATTERN = (
        r"(?P<time>(?P<hour>\d{2}):(?P<minute>\d{2}):(?P<second>\d{2}))"
    )
    DATE_PATTERN = r"(?P<date>(?P<month>\d{2})/(?P<day>\d{2})/(?P<year>\d{2}))"
    DATETIME_PATTERN = (
        r"(?P<date_and_time>" + DATE_PATTERN + r"\s+" + TIME_PATTERN + r")"
    )
    DEWPOINT_PATTERN = (
        r"(?P<dewpoint_assign>DP\s*=\s*(?P<dewpoint>[0-9.-]+)\s*C)"
    )
    TEMPERATURE_PATTERN = (
        r"(?P<temperature_assign>AT\s*=\s*(?P<temperature>[0-9.-]+)\s*C)"
    )
    HUMIDITY_PATTERN = r"(?P<humidity_assign>RH\s*=\s*(?P<humidity>[0-9.]+))"
    DATA_PATTERN = r"\s+".join(
        [DEWPOINT_PATTERN, TEMPERATURE_PATTERN, HUMIDITY_PATTERN]
    )
    MODE_PATTERN = r"(?P<mode>(?:\S+(?:\s\S+)*)+)?"
    LINE_REGEX = re.compile(
        pattern=r"\s*".join([DATETIME_PATTERN, DATA_PATTERN, MODE_PATTERN]),
        flags=re.IGNORECASE,
    )

    @property
    def data(self):
        """
        Device loggin data. This is a Generator, always yielding the next
        dataset. Use :any:`next` to retreive the next dataset in the queue.

        :getter: Reads
        :type: :any:`dict`
        """
        for i, line in enumerate(self):
            logger.debug("<<< {}".format(line))
            try:
                d = self.LINE_REGEX.search(self.bytes2str(line)).groupdict()
                # convert numbers
                d = {k: str2num(v) for k, v in d.items()}
                # parse date
                d["datetime"] = datetime.datetime.strptime(
                    d["date_and_time"], self.DATETIME_FORMAT
                )
                yield d
            except (ValueError, AttributeError):
                if line.strip():  # not just whitespace
                    logger.warning("Could not parse data line {}".format(line))
                else:  # just whitespace
                    logger.debug("Skipping empty line")
                continue

    @classmethod
    def bytes2str(cls, bs):
        """
        Encode a :any:`str` to :any:`bytes` according to the RHCAL

        Args:
            bs (bytes): the bytes to convert

        Returns:
            string : the converted string
        """
        return bs.decode(encoding=cls.ENCODING, errors="ignore")

    @classmethod
    def str2bytes(cls, string):
        """
        Encode a :any:`str` to :any:`bytes` according to the RHCAL

        Args:
            string (str): the string to convert

        Returns:
            bytes : the converted bytes
        """
        return string.encode(encoding=cls.ENCODING, errors="ignore")

    def read_until_regex(self, regex):
        """
        Read from the serial port until ``regex`` matches.

        Args:
            regex (str or _any:_`_sre.SRE_Pattern`): the regex to match

        Returns:
            string : the read string
        """
        regex = regex if hasattr(regex, "search") else re.compile(regex)
        bytes_read = bytes()
        while True:
            tmp = self.read(1)
            bytes_read += tmp
            string_read = self.bytes2str(bytes_read)
            if regex.search(string_read):
                logger.debug(
                    "Read string '{}' matches regex '{}'".format(
                        string_read, regex.pattern
                    )
                )
                break
            else:
                logger.debug(
                    "Read string '{}' does not match regex '{}'".format(
                        string_read, regex.pattern
                    )
                )
        return string_read

    def read_string(self, string, ignore_case=False):
        """
        Read a ``string`` from the serial interface, optionally ignoring case.

        Args:
            string (str): the string to read
            ignore_case (bool, optional): ignore the case?

        Returns:
            bytes : the read bytes

        Raises:
            RHCALException : a read byte did not match
        """
        read_bytes = bytes()
        for char in string:
            tmp = self.read(1)
            read_bytes += tmp
            tmp = tmp.decode()
            matches = (
                tmp.lower() == char.lower() if ignore_case else tmp == char
            )
            if not matches:
                raise RHCALException(
                    "Received byte {} does not match {}".format(tmp, char)
                )
        return read_bytes

    def command(self, cmd, data=None):
        """
        Execute command ``cmd``.

        Args:
            cmd (str): the command to execute
            data (str, optional): optional data for the command
        """
        self.press_enter(2)
        self.flushInput()
        for i, char in enumerate(cmd):
            self.type(char)
        self.press_enter()
        time.sleep(self.RESPONSE_DELAY_SECONDS)
        if data:
            self.type(data)
            self.press_enter()
            time.sleep(self.RESPONSE_DELAY_SECONDS)

    def type(self, content):
        """
        Convenient wrapper around :any:`serial.Serial.write` to simulate typing
        of characters.

        Args:
            content (object): any object to send, will by :any:`str`-ingified.

        Returns:
            string : the real string that has been sent
        """
        # write byte-by-byte
        for char in str(content):
            byte = self.str2bytes(char)
            logger.debug(">>> {}".format(byte))
            self.write(byte)
            self.flushOutput()
            time.sleep(self.TYPE_DELAY_SECONDS)
        return str(content)

    def press_enter(self, n=1):
        """
        Simulate pressing the Return button.

        Args:
            n (int, optional): How many times to press it
        """
        return self.type("\r" * n)


class RHCALException(Exception):
    """
    Base Exception class for RHCAL errors
    """

    pass
