# system modules
import argparse
import logging
import sys
import csv
import datetime
import textwrap
import time
import re
import importlib
import itertools

# internal modules
import RHCAL
from RHCAL.utils import *

# external modules
import serial.tools.list_ports


logger = logging.getLogger("RHCAL cli")


def output_interval(s):
    try:
        i = int(s)
    except BaseException:
        raise argparse.ArgumentTypeError("{} is not an integer".format(s))
    if 1 <= i <= 3600:
        return i
    else:
        raise argparse.ArgumentTypeError(
            "{} is not in range [1;3600]".format(s)
        )


def strftime_format(s):
    if datetime.datetime.utcnow().strftime(s) == s:
        raise argparse.ArgumentTypeError(
            "'{}' does not seem to contain "
            "any strftime-conformant formats".format(s)
        )
    return s


EXPERIMENT_COLUMNS = {
    "start_time": {
        "order": 1,
        "default": "now",
        "description": "wait until this time to start the experiment",
    },
    "end_time": {
        "order": 2,
        "default": "now + max_seconds",
        "description": "the experiment is aborted not later than this time",
    },
    "min_seconds_at_setpoint": {
        "order": 3,
        "default": "∞",
        "description": "min. seconds setpoints should be within tolerances",
    },
    "max_seconds": {
        "order": 4,
        "default": "∞",
        "description": "The experiment is aborted after this many seconds.",
    },
    "temperature_c": {
        "order": 5,
        "default": "no change",
        "description": "the temperature setpoint for this experiment in "
        "degrees celsius",
    },
    "humidity_percent": {
        "order": 6,
        "default": "no change",
        "description": "the humidity setpoint for this experiment in percent",
    },
    "temperature_tolerance_c": {
        "order": 7,
        "default": "∞",
        "description": "the temperature tolerance in degrees celsius",
    },
    "humidity_tolerance_percent": {
        "order": 8,
        "default": "∞",
        "description": "the humidity tolerance in percent",
    },
    "output_interval": {
        "order": 9,
        "default": "no change",
        "description": "the data output interval for this experiment",
    },
    "function": {
        "order": 10,
        "default": "nothing",
        "description": (
            "Python function (like 'mymodule.myfuncname') "
            "with signature func(reason,*args, **kwargs) "
            "to execute at certain stages "
            "(reason = 'before' experiment, in the measurement 'loop', "
            "at 'timeout', 'abort' or 'success')"
        ),
    },
}

parser = argparse.ArgumentParser(
    prog="python3 -m RHCAL",
    formatter_class=argparse.RawTextHelpFormatter,
    description=textwrap.dedent(
        """

    Operate an EdgeTech RH CAL relative humidity calibrator via a serial port

    # A note on setpoints

    The device status (e.g.) temperature and humidity setpoints and the output
    interval are cached for {status_cache} seconds as querying them takes a
    couple of seconds and the device can only either output data or answer a
    query. This means that the setpoints may be wrong for the duration of the
    caching if the setpoints are changed otherwise, e.g. via the buttons on the
    device.

    # Version

    This is version {version}

    """.format(
            status_cache=RHCAL.RHCAL.STATUS_CACHE_SECONDS,
            version=RHCAL.__version__,
        )
    ).strip(),
)


ioargs = parser.add_argument_group("Input/Output")
ioargs.add_argument("-p", "--port", help="The serial port to use.")
ioargs.add_argument(
    "-f",
    "--experiment_file",
    help=textwrap.dedent(
        """
Path to the experiment control file.

This file is a CSV (comma ',' separated values) file with the following
columns (all are optional and have defaults):

{columns}

""".format(
            columns="\n".join(
                [
                    "{col}: (default: {default})\n    {description}".format(
                        col=k, **v
                    )
                    for k, v in sorted(
                        EXPERIMENT_COLUMNS.items(), key=lambda x: x[1]["order"]
                    )
                ]
            )
        )
    ).strip(),
    type=argparse.FileType("r"),
)
ioargs.add_argument("-o", "--output", help="Output CSV file")

devinter = parser.add_argument_group("Device settings")
devinter.add_argument(
    "-b",
    "--baudrate",
    help="The baudrate to use. "
    "Defaults to the highest baudrate {}.".format(max(RHCAL.RHCAL.BAUDRATES)),
    type=int,
    choices=RHCAL.RHCAL.BAUDRATES,
    default=max(RHCAL.RHCAL.BAUDRATES),
)
devinter.add_argument(
    "-i",
    "--interval",
    help="Default output interval in seconds",
    type=output_interval,
)
devinter.add_argument(
    "-t", "--timeout", help="Serial read timeout (seconds).", type=float
)
devinter.add_argument(
    "--set_time", help="Set device time to system time.", action="store_true"
)
devinter.add_argument(
    "--log_abc",
    help="[ignored] Also log data during ABC cycle",
    action="store_true",
)
devinter.add_argument(
    "--no-log-abc", help="Don't log data during ABC cycle", action="store_true"
)

formats = parser.add_argument_group("Formatting")
formats.add_argument(
    "--localtime", help="Use local time instead of UTC", action="store_true"
)
formats.add_argument(
    "--time_format",
    help="strftime-conform time format to " "use for all time parsing/output",
    type=strftime_format,
    default="%Y-%m-%d %H:%M:%S",
)
formats.add_argument(
    "-c", "--comment_char", help="Comment character", default="#"
)
formats.add_argument(
    "--no_comments", help="Suppress comments in output", action="store_true"
)


verbosity = parser.add_mutually_exclusive_group()
verbosity.add_argument(
    "-v", "--verbose", help="verbose output", action="store_true"
)
verbosity.add_argument(
    "-q", "--quiet", help="only errorneous output", action="store_true"
)
parser.add_argument(
    "-n",
    "--dry_run",
    help="Don't actually interfere " "with the device at all, just simulate.",
    action="store_true",
)
verbosity.add_argument(
    "--version", help="show version and exit", action="store_true"
)


def main():
    args = parser.parse_args()

    if args.version:
        print(RHCAL.__version__)
        sys.exit(0)

    loglevel = logging.INFO
    if args.verbose:
        loglevel = logging.DEBUG
    if args.quiet:
        loglevel = logging.WARNING
    logging.basicConfig(
        level=loglevel, format="[%(asctime)s] - %(levelname)-8s - %(message)s"
    )
    for name, lgr in logging.Logger.manager.loggerDict.items():
        if "rhcal" in name.lower() or "root" in name:
            lgr.setLevel(loglevel)

    time_log_col = "time_log" + ("_local" if args.localtime else "_utc")

    # create the RHCAL object
    if args.dry_run:
        import random
        from unittest import mock

        logger.warning(
            "Performing dry run. None of the following actions are "
            "actually preformed and all displayed data is fake."
        )
        rhcal = mock.Mock()  # the rhcal object is a Mock object

        # mock the RHCAL.RHCAL.temperature_setpoint property
        setter_mock = mock.Mock(wraps=RHCAL.RHCAL.temperature_setpoint.fset)
        mock_property = RHCAL.RHCAL.temperature_setpoint.setter(setter_mock)
        mock.patch.object(
            RHCAL.RHCAL, "temperature_setpoint", mock_property
        ).start()
        rhcal.temperature_setpoint = 999  # must provide initial value

        # mock the RHCAL.RHCAL.humidity_setpoint property
        setter_mock = mock.Mock(wraps=RHCAL.RHCAL.humidity_setpoint.fset)
        mock_property = RHCAL.RHCAL.humidity_setpoint.setter(setter_mock)
        mock.patch.object(
            RHCAL.RHCAL, "humidity_setpoint", mock_property
        ).start()
        rhcal.humidity_setpoint = 999  # must provide initial value

        # mock the RHCAL.RHCAL.output_interval property
        setter_mock = mock.Mock(wraps=RHCAL.RHCAL.output_interval.fset)
        mock_property = RHCAL.RHCAL.output_interval.setter(setter_mock)
        mock.patch.object(
            RHCAL.RHCAL, "output_interval", mock_property
        ).start()
        rhcal.output_interval = 30  # must provide initial value

        # mock the RHCAL.RHCAL.data property
        type(rhcal).data = mock.PropertyMock(
            return_value=(
                {
                    "datetime": now(),
                    "temperature": random.randrange(start=100, stop=500) / 10,
                    "humidity": random.randrange(start=0, stop=1000) / 10,
                    "dewpoint": random.randrange(start=100, stop=500) / 10,
                    "mode": "",
                    "time": time.sleep(rhcal.output_interval),
                }
                for i in itertools.repeat(1)
            )
        )
    else:
        port = args.port
        if not port:
            serial_ports = serial.tools.list_ports.comports()
            if len(serial_ports) == 1:
                port = port = serial_ports[0].device
            elif not serial_ports:
                parser.error(
                    "No serial ports on the system. "
                    "Use -n/--dry_run to simulate."
                )
            elif len(serial_ports) > 1:
                parser.error(
                    f"{len(serial_ports)} serial ports on the system "
                    f"({', '.join([repr(p.device) for p in serial_ports])}). "
                    "Select one with `-p PORT` "
                    "or use -n/--dry_run to simulate."
                )
        rhcal = RHCAL.RHCAL(
            port=port,
            baudrate=args.baudrate,
            timeout=args.timeout,
        )
    rhcal.press_enter(3)

    def now():
        return (
            datetime.datetime.now()
            if args.localtime
            else datetime.datetime.utcnow()
        )

    if args.set_time:
        if args.localtime:
            logger.info("Setting device time to system local time...")
        else:
            logger.info("Setting device time to system UTC time...")
        rhcal.time = now()

    if args.interval:
        if rhcal.output_interval != args.interval:
            logger.info(
                "Setting device output interval to {} seconds...".format(
                    args.interval
                )
            )
            rhcal.output_interval = args.interval
        else:
            logger.info(
                "Device output interval is already at {} seconds.".format(
                    args.interval
                )
            )

    outfile = (
        sys.stdout
        if args.dry_run
        else (open(args.output, "w") if args.output else sys.stdout)
    )
    csvwriter = csv.DictWriter(
        outfile,
        fieldnames=[
            time_log_col,
            "time_rhcal",
            "exp_nr",
            "temp_c",
            "temp_setpoint_c",
            "dewpoint_c",
            "hum_percent",
            "hum_setpoint_percent",
            "mode",
        ],
    )
    logger.debug("Command-line: {}".format(" ".join(sys.argv)))
    if not args.dry_run:
        if not args.no_comments:
            outfile.write(
                "{} {}\n".format(args.comment_char, " ".join(sys.argv))
            )
        csvwriter.writeheader()
        outfile.flush()

    def csv_dataset(data):
        return {
            time_log_col: now().strftime(args.time_format),
            "time_rhcal": data.get("datetime").strftime(args.time_format),
            "temp_c": data.get("temperature"),
            "temp_setpoint_c": rhcal.temperature_setpoint,
            "hum_percent": data.get("humidity"),
            "hum_setpoint_percent": rhcal.humidity_setpoint,
            "dewpoint_c": data.get("dewpoint"),
            "mode": data.get("mode"),
        }

    # an experiment file was specified
    if args.experiment_file:
        # read the experiments line-by-line as CSV
        csvreader = csv.DictReader(
            # filter out comment lines
            filter(
                lambda row: not row.startswith(args.comment_char)
                and len(row.strip()),
                args.experiment_file,
            ),
            escapechar="\\",
        )
        for i, step in enumerate(csvreader):
            try:
                nr = i + 1
                info = {}
                info["temp_wi_tolerance"] = False
                info["hum_wi_tolerance"] = False
                info["wi_tolerance"] = False
                info["earliest_ETA"] = datetime.datetime.max
                info["latest_ETA"] = datetime.datetime.max
                info["seconds_at_setpoint"] = 0
                info["warned_about_infinity"] = False
                logger.info("Experiment Nr. {}".format(nr))
                if not args.no_comments:
                    outfile.write(
                        "{} Experiment Nr. {}\n".format(args.comment_char, nr)
                    )
                    outfile.flush()

                function_path = step.get("function")
                if function_path:
                    try:
                        module_name, function_name = re.fullmatch(
                            r"(.*?)\.([^.]+)", function_path
                        ).groups()
                        module = importlib.import_module(module_name)
                        function = getattr(module, function_name)
                    except Exception as e:
                        logger.error(
                            f"{function_path!r} does not look like a "
                            f"path to a function: {type(e).__name__}: {e}"
                        )
                        function = None
                else:
                    function = None

                def run_function(*a, **kwargs):
                    if not function:
                        return
                    arguments = ", ".join(
                        itertools.chain(
                            map(repr, a),
                            (f"{k}={v!r}" for k, v in kwargs.items()),
                        )
                    )
                    logger.debug(
                        f"Running function "
                        f"{function.__module__}.{function.__name__}"
                        f"({arguments})..."
                    )
                    if args.dry_run:
                        return
                    try:
                        ret = function(*a, **kwargs)
                        logger.debug(
                            f"{step['function']!r}({arguments}) "
                            f"returned {ret!r}"
                        )
                    except Exception as e:
                        logger.error(
                            f"Error running function "
                            f"{step['function']!r}({arguments}): "
                            f"{type(e).__name__}: {e}"
                        )

                # convert numbers
                step = {k: str2num(v) for k, v in step.items()}
                logger.debug("Experiment settings: {}".format(step))
                run_function("before", nr, step, info)
                # start time
                start_time = step.get("start_time")
                if start_time:
                    try:
                        start_time = parse_date(start_time, args.time_format)
                    except ValueError:
                        logger.warning(
                            "start_time '{}' does not look like time "
                            "formatted like '{}'".format(
                                start_time, args.time_format
                            )
                        )
                    seconds = (start_time - now()).total_seconds()
                    if seconds > 0:
                        logger.info(
                            "Waiting {:.0f} seconds until {} to begin the "
                            "experiment".format(
                                seconds, start_time.strftime(args.time_format)
                            )
                        )
                        if not args.dry_run:
                            time.sleep(seconds)
                    else:
                        logger.info(
                            "{} is in the past, no need to wait.".format(
                                start_time.strftime(args.time_format)
                            )
                        )
                # output interval
                output_interval = step.get("output_interval")
                if output_interval:
                    if output_interval == rhcal.output_interval:
                        logger.info(
                            "Output interval is already at {} seconds".format(
                                rhcal.output_interval
                            )
                        )
                    else:
                        logger.info(
                            "Setting device output "
                            "interval to {} seconds...".format(output_interval)
                        )
                        rhcal.output_interval = output_interval
                # temperature
                temperature_c = step.get("temperature_c")
                if temperature_c:
                    if temperature_c == rhcal.temperature_setpoint:
                        logger.info(
                            "Temperature setpoint is already at {}°C".format(
                                rhcal.temperature_setpoint
                            )
                        )
                    else:
                        logger.info(
                            "Setting temperature setpoint to {}°C".format(
                                temperature_c
                            )
                        )
                        rhcal.temperature_setpoint = temperature_c
                # humidity
                humidity_percent = step.get("humidity_percent")
                if humidity_percent:
                    if humidity_percent == rhcal.humidity_setpoint:
                        logger.info(
                            "Humidity setpoint is already at {}%".format(
                                rhcal.humidity_setpoint
                            )
                        )
                    else:
                        logger.info(
                            "Setting humidity setpoint to {}%".format(
                                humidity_percent
                            )
                        )
                        rhcal.humidity_setpoint = humidity_percent
                # logging
                temperature_tolerance = step.get(
                    "temperature_tolerance_c", float("inf")
                ) or float("inf")
                humidity_tolerance = step.get(
                    "humidity_tolerance_percent", float("inf")
                ) or float("inf")
                max_seconds = step.get("max_seconds", float("inf")) or float(
                    "inf"
                )
                end_time = (
                    parse_date(str(step.get("end_time", "")), args.time_format)
                    or datetime.datetime.max
                )
                min_seconds_at_setpoint = step.get(
                    "min_seconds_at_setpoint", float("inf")
                ) or float("inf")
                logger.info(
                    "Will now log data until temperature "
                    "({temp}±{temp_tol}°C) and "
                    "humidity ({hum}±{hum_tol}%) are at "
                    "least {min_sec} seconds within their "
                    "setpoint tolerances (but at most"
                    " {max_sec} seconds) or until {end_time}...".format(
                        hum=rhcal.humidity_setpoint,
                        temp=rhcal.temperature_setpoint,
                        hum_tol=humidity_tolerance,
                        temp_tol=temperature_tolerance,
                        min_sec="an infinity of"
                        if min_seconds_at_setpoint == float("inf")
                        else min_seconds_at_setpoint,
                        max_sec="an infinity of"
                        if max_seconds == float("inf")
                        else max_seconds,
                        end_time=end_time,
                    )
                )
                info["logging_since"] = now()
                while True:
                    # overwrite last state
                    info["temp_prev_wi_tolerance"] = info["temp_wi_tolerance"]
                    info["hum_prev_wi_tolerance"] = info["hum_wi_tolerance"]
                    info["prev_wi_tolerance"] = info["wi_tolerance"]
                    info["prev_earliest_ETA"] = info["earliest_ETA"]
                    info["prev_latest_ETA"] = info["latest_ETA"]
                    # don't do any of this if we are in dry run mode
                    if args.dry_run:
                        break
                    run_function("loop", nr, step, info)
                    # check if maximum timeout is reached
                    # TODO: This part is actually never executed due to the
                    # remaining time handling below...
                    logging_seconds = (
                        now() - info["logging_since"]
                    ).total_seconds()
                    if not max_seconds == float("inf"):
                        info["earliest_ETA"] = min(
                            info["earliest_ETA"],
                            now() + datetime.timedelta(seconds=max_seconds),
                        )
                        info["latest_ETA"] = max(
                            info["latest_ETA"],
                            now() + datetime.timedelta(seconds=max_seconds),
                        )
                    if logging_seconds > max_seconds:
                        logger.warning(
                            "This step has been going on for {:.0f} "
                            "seconds now. This is too long... "
                            "Aborting experiment Nr. {}".format(
                                info["seconds_at_setpoint"], nr
                            )
                        )
                        run_function("timeout", nr, step, info)
                        break
                    # check if end time is reached
                    info["earliest_ETA"] = min(info["earliest_ETA"], end_time)
                    info["latest_ETA"] = max(info["earliest_ETA"], end_time)
                    if end_time:
                        if now() > end_time:
                            logger.warning(
                                "It is later than {}. "
                                "Finishing this step.".format(end_time)
                            )
                            run_function("timeout", nr, step, info)
                            break
                    seconds_remaining = (
                        info["earliest_ETA"] - now()
                    ).total_seconds()
                    if seconds_remaining < rhcal.output_interval:
                        # retreiving the dataset would take longer than the ETA
                        if info["wi_tolerance"]:
                            logger.info(
                                "Retreiving the next dataset would excess the "
                                "estimated minimum remainig experiment "
                                "time of {:.1f} seconds. "
                                "Just waiting the remainig time...".format(
                                    seconds_remaining
                                )
                            )
                        else:
                            logger.warning(
                                "Retreiving the next dataset would excess the "
                                "estimated minimum remainig experiment "
                                "time of {:.1f} seconds. "
                                "Just waiting the remainig time and "
                                "then abort this experiment Nr. {}".format(
                                    seconds_remaining, nr
                                )
                            )
                        run_function("timeout", nr, step, info)
                        break
                    else:
                        # retreive dataset
                        data = next(rhcal.data)
                        mode = data.get("mode", "") or ""
                        if "abc" in mode.lower():
                            logger.warning(
                                "The device is performing an ABC-Cycle! "
                                "Halting experiment Nr. {}...".format(nr)
                            )
                            # read data until ABC is over
                            while True:
                                data = next(rhcal.data)
                                if (
                                    "abc"
                                    not in (data.get("mode") or "").lower()
                                ):
                                    break
                                # output if desired
                                if not args.no_log_abc:
                                    # convert
                                    row = csv_dataset(data)
                                    # add the experiment Nr.
                                    row.update({"exp_nr": nr})
                                    run_function("data", nr, step, info, row)
                                    # write data
                                    csvwriter.writerow(row)
                                    outfile.flush()
                            logger.warning(
                                "The device is done with the ABC-Cycle! "
                                "Continuing experiment Nr. {}".format(nr)
                            )
                            logger.warning(
                                "Resetting time for experiment Nr. {}".format(
                                    nr
                                )
                            )
                            info["logging_since"] = now()
                    # extract data
                    temperature = data.get("temperature", float("-inf"))
                    humidity = data.get("humidity", float("-inf"))
                    info["temp_wi_tolerance"] = abs(
                        temperature_tolerance
                    ) + sys.float_info.epsilon * 1000 >= abs(
                        temperature - rhcal.temperature_setpoint
                    )
                    info["hum_wi_tolerance"] = abs(
                        humidity_tolerance
                    ) + sys.float_info.epsilon * 1000 >= abs(
                        humidity - rhcal.humidity_setpoint
                    )
                    info["wi_tolerance"] = (
                        info["temp_wi_tolerance"] and info["hum_wi_tolerance"]
                    )
                    # temperature wihin tolerance state changed
                    if (
                        info["temp_wi_tolerance"]
                        != info["temp_prev_wi_tolerance"]
                    ):
                        # temperature is now within tolerance
                        if info["temp_wi_tolerance"]:
                            logger.info(
                                "Temperature {} °C is now within ±{} °C "
                                "tolerance".format(
                                    temperature, temperature_tolerance
                                )
                            )
                        # temperature is not anymore within tolerance
                        else:
                            logger.info(
                                "Temperature {} °C is not anymore within {} °C"
                                " tolerance".format(
                                    temperature, temperature_tolerance
                                )
                            )
                    # humidity wihin tolerance state changed
                    if (
                        info["hum_wi_tolerance"]
                        != info["hum_prev_wi_tolerance"]
                    ):
                        # humidity is now within tolerance
                        if info["hum_wi_tolerance"]:
                            logger.info(
                                "Humidity {} % is now within ±{}% "
                                "tolerance".format(
                                    humidity, humidity_tolerance
                                )
                            )
                        # humidity is not anymore within tolerance
                        else:
                            logger.info(
                                "Humidity {}% is not anymore within "
                                "±{}% tolerance".format(
                                    humidity, humidity_tolerance
                                )
                            )
                    # we are currently within the tolerance
                    if info["wi_tolerance"]:
                        if min_seconds_at_setpoint != float("inf"):
                            info["earliest_ETA"] = min(
                                info["earliest_ETA"],
                                now()
                                + datetime.timedelta(
                                    seconds=min_seconds_at_setpoint
                                ),
                            )
                            info["latest_ETA"] = max(
                                info["latest_ETA"],
                                now()
                                + datetime.timedelta(
                                    seconds=min_seconds_at_setpoint
                                ),
                            )
                        # within tolerance state changed
                        if info["wi_tolerance"] != info["prev_wi_tolerance"]:
                            logger.info(
                                "We are now within the tolerances. "
                                "If this holds, we are done with experiment "
                                "Nr. {} in max. {} seconds".format(
                                    nr, min_seconds_at_setpoint
                                )
                            )
                        # make sure there is wi_tolerance_since key
                        info["wi_tolerance_since"] = (
                            info.pop("wi_tolerance_since", None) or now()
                        )
                        # check if timeout is reached
                        info["seconds_at_setpoint"] = (
                            now() - info["wi_tolerance_since"]
                        ).total_seconds()
                        # minimum time at setpoint reached
                        if (
                            info["seconds_at_setpoint"]
                            > min_seconds_at_setpoint
                        ):
                            logger.info(
                                "We have been within the setpoint tolerances "
                                "for {:.0f} seconds now. Finishing this "
                                "experiment Nr.  {}!".format(
                                    info["seconds_at_setpoint"], nr
                                )
                            )
                            run_function("success", nr, step, info)
                            break
                    else:
                        # within tolerance state changed
                        if info["wi_tolerance"] != info["prev_wi_tolerance"]:
                            logger.info(
                                "We are not anymore within the tolerances! "
                                "Resetting the timer."
                            )
                        # reset within tolerance timer
                        info.pop("wi_tolerance_since", None)
                    # convert dataset to CSV dict
                    row = csv_dataset(data)
                    # add the experiment Nr.
                    row.update({"exp_nr": nr})
                    # write data
                    csvwriter.writerow(row)
                    outfile.flush()
                    # check ETA
                    if (
                        info["earliest_ETA"] == datetime.datetime.max
                        and not info["warned_about_infinity"]
                    ):
                        logger.warning(
                            "This experiment Nr. {} might "
                            "go on forever!".format(nr)
                        )
                        info["warned_about_infinity"] = True
                    ETA_changed = (
                        info["earliest_ETA"] != info["prev_earliest_ETA"]
                        or info["latest_ETA"] != info["prev_latest_ETA"]
                    )
                    if ETA_changed:
                        min_seconds_remaining = (
                            info["earliest_ETA"] - now()
                        ).total_seconds()
                        max_seconds_remaining = (
                            info["latest_ETA"] - now()
                        ).total_seconds()
                        if info["earliest_ETA"] == info["latest_ETA"]:
                            logger.info(
                                "We are done with experiment Nr. {nr} in "
                                "{min_sec:.0f} seconds at {min_ETA}".format(
                                    nr=nr,
                                    min_ETA=info["earliest_ETA"].strftime(
                                        args.time_format
                                    ),
                                    min_sec=min_seconds_remaining,
                                )
                            )
                        else:
                            logger.info(
                                "We are done with experiment Nr. {nr} between "
                                "{min_ETA} and {max_ETA} "
                                "({min_sec:.0f} to {max_sec:.0f}) "
                                "seconds.".format(
                                    nr=nr,
                                    min_ETA=info["earliest_ETA"].strftime(
                                        args.time_format
                                    ),
                                    max_ETA=info["latest_ETA"].strftime(
                                        args.time_format
                                    ),
                                    min_sec=min_seconds_remaining,
                                    max_sec=max_seconds_remaining,
                                )
                            )
            except KeyboardInterrupt:
                logger.info(
                    "Received KeyboardInterrupt. "
                    "Aborting experiment Nr. {}".format(nr)
                )
                run_function("abort", nr, step, info)
                continue
    # no experiment file
    else:
        logger.info("Will now log data from the device forever.")
        if not args.dry_run:
            # log for infinity
            try:
                while True:
                    row = csv_dataset(next(rhcal.data))
                    csvwriter.writerow(row)
                    outfile.flush()
            # stop on STRG-C
            except KeyboardInterrupt:
                pass

    # close output file
    if outfile is not sys.stdout:
        outfile.close()


if __name__ == "__main__":
    main()
