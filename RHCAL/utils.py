# system modules
import itertools
import sys
import datetime

# external modules


def str2num(s):
    """
    Convert a :any:`str` to either an :any:`int` or a :any:`float`

    Args:
        s (str): the string to convert

    Returns:
        int : if the string could be converted to an integer
        float : if the string could be converted to a float
        string : otherwise
        None : if ``s`` is :any:`None`
    """
    if s is None:
        return s
    try:
        i = int(s)
    except ValueError:
        i = None
    try:
        f = float(s)
    except ValueError:
        f = None
    return s if (i is None and f is None) else (i if i == f else f)


def all_substrings(string):
    """
    Generator to get all substrings from a given string.

    Taken from https://stackoverflow.com/a/22470158
    """
    length = len(string)
    for i in range(length):
        for j in range(i + 1, length + 1):
            yield (string[i:j])


def parse_date(datestring, fullfmt):
    """
    Tries harder to :any:`strptime` a ``datestring`` by also trying
    :any:`all_substrings` of ``fullfmt``.

    Args:
        datestring (str): the date string to parse
        fullfmt (str): the full date format

    Returns:
        datetime.datetime : corresponding to the first sliced format that
            matched
        None : if no format slice matched
    """
    for fmt in itertools.chain(fullfmt, all_substrings(fullfmt)):
        try:
            date = datetime.datetime.strptime(datestring, fmt)
            if (
                date.date()
                == datetime.datetime.strptime(  # bogus date
                    date.time().strftime("%H:%M:%S"), "%H:%M:%S"
                ).date()
            ):
                # set current date
                date = datetime.datetime.combine(
                    datetime.date.today(), date.time()
                )
            return date
        except ValueError:
            continue
